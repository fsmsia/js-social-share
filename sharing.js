

function popup(url) {
    window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
}

function description(){
    return $(this).attr('data-description') !== undefined ? $(this).attr('data-description') : document.querySelector('meta[name=Description]').content;
}

function title(){
    return $(this).attr('data-title') !== undefined ? $(this).attr('data-title') : $('title').text();
}

function image(){
    return $(this).attr('data-image') !== undefined ? $(this).attr('data-image') : '';
}

function getUrl(){
    return $(this).attr('data-url') !== undefined ? $(this).attr('data-url') : location.href;
}

$(document).on('click','.shareFacebook',function () {
        var url = 'https://www.facebook.com/share.php?u='+ document.querySelector('meta[name="fb_url"]').content+'&title='+ document.querySelector('meta[name="fb_title"]').content+'';
        if (document.querySelector('meta[name="fb_image"]')){
            url += '&picture='+ document.querySelector('meta[name="fb_image"]').content;
        }
        popup(url);
    return false;
});

$(document).on('click','.shareTwitter',function () {
    url  = 'http://twitter.com/share?';
    url += 'text=' + encodeURIComponent(description());
    url += '&url=' + getUrl();
    popup(url);
    return false;
});

$(document).on('click','.sharePinterest',function () {
    url = 'https://pinterest.com/pin/create/button/?';
    url += 'url=' + getUrl() + '#';
    url += '&media=' + encodeURIComponent(image());
    url += '&description=' + encodeURIComponent(description());
    popup(url);
    return false;
});


$(document).on('click','.shareDraugiem',function () {
    url = 'http://www.draugiem.lv/say/ext/add.php?';
    url += '?title=' + encodeURIComponent( title() );
    url += '&link=' + encodeURIComponent( getUrl() );
    url +=  description() ? '&titlePrefix=' + encodeURIComponent( description() ) : '';
    popup(url);
    return false;
});


$(document).on('click','.shareGoogle',function () {
    url = 'https://plus.google.com/share?';
    url += 'url=' + getUrl();
    popup(url);
    return false;
});

var getWindowOptions = function() {
    var width = 626;
    var height = 436;
    var left = (window.innerWidth / 2) - (width / 2);
    var top = (window.innerHeight / 2) - (height / 2);

    return [
      'resizable,scrollbars,status',
      'height=' + height,
      'width=' + width,
      'left=' + left,
      'top=' + top,
    ].join();
};

var fbBtn = document.querySelector('.facebook-share');
if (fbBtn) {
    var title = encodeURIComponent(fbBtn.dataset.title);
    var picture = encodeURIComponent(fbBtn.dataset.picture);
    var shareUrl = 'https://www.facebook.com/sharer/sharer.php?u=' + location.href + '&title=' + title  + '&picture=' + location.href + '/' + picture;
    fbBtn.href = shareUrl; // 1
    fbBtn.addEventListener('click', function(e) {
    e.preventDefault();
    var win = window.open(shareUrl, 'ShareOnFb', getWindowOptions());
    win.opener = null; // 2
    });
}


